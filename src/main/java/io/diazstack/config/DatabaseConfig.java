package io.diazstack.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories("io.diazstack.repository")
@EnableTransactionManagement
public class DatabaseConfig {

			
}//class
