package io.diazstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDialerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDialerApplication.class, args);
	}

}
