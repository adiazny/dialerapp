package io.diazstack.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class DialerResult {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate dateCreated;
	
	private String macAddress;
	private String ipAddress;
	private String dn;
	private String beforeImage;
	private String afterImage;
	private boolean isCallSuccessful;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDate getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public String getBeforeImage() {
		return beforeImage;
	}
	public void setBeforeImage(String beforeImage) {
		this.beforeImage = beforeImage;
	}
	public String getAfterImage() {
		return afterImage;
	}
	public void setAfterImage(String afterImage) {
		this.afterImage = afterImage;
	}
	public boolean isCallSuccessful() {
		return isCallSuccessful;
	}
	public void setCallSuccessful(boolean isCallSuccessful) {
		this.isCallSuccessful = isCallSuccessful;
	}
	
	

}//class
