package io.diazstack.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.diazstack.domain.DialerResult;
import io.diazstack.repository.DialerResultRepository;

@Service
public class DialerResultService {
	
	@Autowired
	private DialerResultRepository dialerResultRepository;
	
	public void addDialerResult(DialerResult dialerResult) {
		dialerResultRepository.save(dialerResult);
	}//addDialerResult()
	
	public List<DialerResult> getAllDialerResults() {
		return dialerResultRepository.findAll();
	}//getAllDialerResults()
	
	public DialerResult getDialerResultByMacAddress(String macAddress) {
		return dialerResultRepository.findByMacAddress(macAddress);
	}//getDialerResultByMacAddress()
	
	public DialerResult getDialerResultByIpAddress(String ipAddress) {
		return dialerResultRepository.findByIpAddress(ipAddress);
	}//getDialerResultByIpAddress()
	
	public void deleteDialerResult(DialerResult dialerResult) {
		dialerResultRepository.delete(dialerResult);
		
	}//deleteDialerResult()
	

}//class
