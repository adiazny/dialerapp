package io.diazstack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.diazstack.domain.DialerResult;
import io.diazstack.service.DialerResultService;


@RestController
@RequestMapping(ResourceConstants.DIALER_RESULT_V1)
public class DialerResultController {

	@Autowired
	private DialerResultService dialerResultService;
	
	@PostMapping("/add")
	public void addDialerResult(@RequestBody DialerResult dialerResult) {
		dialerResultService.addDialerResult(dialerResult);
	}//addDialerResult()
	
	@GetMapping("/all")
	public List<DialerResult> getAllDialerResults(){
		return dialerResultService.getAllDialerResults();
	}//getAllDialerResults()
	
	@GetMapping("/mac/{macAddress}")
	public DialerResult getDialerResultByMacAddress(@PathVariable String macAddress){
		return dialerResultService.getDialerResultByMacAddress(macAddress);
	}//getDialerResultByMacAddress()
	
	@GetMapping("/ip/{ipAddress}")
	public DialerResult getDialerResultByIpAddress(@PathVariable String ipAddress){
		return dialerResultService.getDialerResultByIpAddress(ipAddress);
	}//getDialerResultByIpAddress()

	//This api endpoint needs work.
	/*@DeleteMapping("/delete/{macaddress}")
	public void deleteDialerResult(DialerResult dialerResult) {
		dialerResultService.deleteDialerResult(dialerResult);
	}//deleteDialerResult()*/
	
	
}//class
