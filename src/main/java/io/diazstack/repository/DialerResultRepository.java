package io.diazstack.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import io.diazstack.domain.DialerResult;

public interface DialerResultRepository extends JpaRepository<DialerResult, String>{

	DialerResult findByMacAddress(String macAddress);
	DialerResult findByIpAddress(String ipAddress);
	

}//class